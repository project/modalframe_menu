<?php

/**
 * @file
 * Allow loading menu item targets in a modal frame using Modal Frame API
 */


/**
 * Implementation of hook_init().
 */
function modalframe_menu_init() {
  //if we're in a modal frame
  if (modalframe_mlid()) {
    // Send the Modal Frame javascript for child windows to the page (also forces rendering of stripped down page)
    modalframe_child_js();
    drupal_add_js(drupal_get_path('module', 'modalframe_menu').'/js/modalframe_menu_child.js');
    //if we should close the current frame due to the page it is currently showing
    $options = modalframe_menu_get_options(modalframe_mlid());
    if ($options['closepages']) {
      $path = drupal_get_path_alias($_GET['q']);
      if (drupal_match_path($path, $options['closepages']) || ($path != $_GET['q'] && drupal_match_path($_GET['q'], $options['closepages']))) {
        modalframe_menu_close_frame($options['closemessage']);
      }
    }
  } else {
    // Send the Modal Frame javascript for parent windows to the page
    //@TODO load only when necessary (not much harm in including it on every page for now).
    modalframe_parent_js();
    drupal_add_js(drupal_get_path('module', 'modalframe_menu').'/js/modalframe_menu_parent.js');
  }
}

/**
 * Implementation of hook_exit().
 * If the current request came from a modal frame then make sure drupal_goto URLs include the mfmlid GET variable.
 */
function modalframe_menu_exit($destination = NULL) {
  if ($destination && modalframe_mlid() && strpos($destination, 'mfmlid') === FALSE) {
    $destination = $destination. (strpos($destination, '?') === FALSE ? '?' : '&').'mfmlid='. modalframe_mlid();
    //if hook_exit was called from drupal_goto then destination settings have already been processed.
    //drupal_goto is the only function in Drupal core that invokes hook_exit on all modules with the $destination argument
    unset ($_REQUEST['destination']);
    unset ($_REQUEST['edit']['destination']);
    drupal_goto($destination);
  }
}

/**
 * Returns the menu link ID (mlid) iff the current page is being loaded in a modal dialog, otherwise FALSE.
 */
function modalframe_mlid() {
  static $modalframe_mlid;
  if (!isset ($modalframe_mlid)) {
    $modalframe_mlid = isset ($_REQUEST['mfmlid']) ? $_REQUEST['mfmlid'] : FALSE;
  }
  return $modalframe_mlid;
}

/**
 * Implementation of hook_form_alter().
 *
 * @see modalframe_menu_form_submit()
 */
function modalframe_menu_form_alter(& $form, $form_state, $form_id) {
  if ($form_id == 'menu_edit_item') {
    $options = modalframe_menu_get_options($form['menu']['mlid']['#value']);

    $form['modalframe'] = array (
      '#type' => 'fieldset',
      '#weight' => 5,
      '#title' => t('Modal frame settings'),
      '#attributes' => array (
        'class' => 'theme-settings-bottom'
      ),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => !$options['enable'],

      
    );
    $form['modalframe']['enable'] = array (
      '#type' => 'checkbox',
      '#title' => t('Open in a modal frame'),
      '#default_value' => $options['enable'],

      
    );
    $form['modalframe']['closebuttons'] = array (
      '#type' => 'textarea',
      '#title' => t('Buttons to close on'),
      '#default_value' => isset ($options['closebuttons']) ? $options['closebuttons'] : t('Save'),
      '#description' => t("If the page contains a form, list form buttons here, one per line, which when clicked should close the frame (as long as form validation didn't fail)."),

      
    );
    $form['modalframe']['closepages'] = array (
      '#type' => 'textarea',
      '#title' => t('Pages to close on'),
      '#default_value' => isset ($options['closepages']) ? $options['closepages'] : '<front>',
      '#description' => t("If the frame should be closed when certain pages are loaded in it, list the page addresses here as Drupal paths one per line. Use <front> to specify the front page. The * character is a wildcard."),

      
    );
    $form['modalframe']['closemessage'] = array (
      '#type' => 'textfield',
      '#title' => t('Close message'),
      '#default_value' => $options['closemessage'],
      '#description' => t("Message to display when the frame is closed."),

      
    );
    $form['modalframe']['hidebuttons'] = array (
      '#type' => 'textarea',
      '#title' => t('Buttons to remove'),
      '#default_value' => $options['hidebuttons'],
      '#description' => t("If the page contains a form, list form buttons here, one per line, which should be removed."),

      
    );
    $form['modalframe']['size'] = array (
      '#type' => 'textfield',
      '#title' => t('Frame size'),
      '#default_value' => $options['size'],
      '#description' => t("Size of the frame in pixels in the format 'widthxheight' (eg '500x500'), leave blank for frame to automatically fit to content."),

      
    );

    $form['submit']['#weight'] = 9;
    $form['delete']['#weight'] = 10;

    $form['#validate'][] = 'modalframe_menu_edit_menu_item_form_validate';
    $form['#submit'][] = 'modalframe_menu_edit_menu_item_form_submit';
  }

  //if current request is in a modal frame
  if (modalframe_mlid()) {
    $options = modalframe_menu_get_options(modalframe_mlid());

    //Add a hidden form element so we know this form was submitted from within a frame.
    //We don't use a form storage/value element as we can't get at the form info from 
    //the hook_init or modalframe_mlid functions (and form submission processing hooks 
    //are too late). Also, JS could also use this info if it's in a hidden form element.
    $form['mfmlid'] = array (
      '#type' => 'hidden',
      '#value' => modalframe_mlid(),

      
    );

    //hide buttons if requested
    if ($options['hidebuttons']) {
      foreach (explode("\n", $options['hidebuttons']) as $button) {
        $button = t(trim($button));
        //search for button recursively in form
        modalframe_menu_form_alter_recursive($form, 'hidebutton', $button);
      }
    }

    //Add submit handler to process possible frame close depending on which button was clicked
    //(add it to all submit handler lists)
    modalframe_menu_form_alter_recursive($form, 'submit');
  }
}

/**
 * Find and alter form elements recursively.
 * @ingroup forms
 */
function modalframe_menu_form_alter_recursive(&$elements, $action, $arg = NULL) {
  // Recurse through all children elements.
  foreach (element_children($elements) as $key) {
    if (isset ($elements[$key]) && $elements[$key]) {
      modalframe_menu_form_alter_recursive($elements[$key], $action, $arg);
    }

    //if we should hide a certain button (given in $arg)
    if ($action == 'hidebutton') {
      if (isset ($elements[$key]['#type']) //defines a form element
          && in_array($elements[$key]['#type'], array ('button', 'image_button', 'submit')) //is a button
          && $elements[$key]['#value'] == $arg) { //is the button we're looking for
        unset ($elements[$key]);
      }
    }
  }
  
  if ($action == 'submit') {
    // If this element has submit handlers, then append our own.
    if (isset ($elements['#submit'])) {
      $elements['#submit'][] = 'modalframe_menu_form_submit';
    }
  }
}

/**
 * Validate the submitted menu item edit form.
 */
function modalframe_menu_edit_menu_item_form_validate($form, & $form_state) {
  $size = $form_state['values']['modalframe']['size'];
  if ($size && !preg_match("/^\d*x\d*$/", $size)) {
    form_set_error('modalframe][size', t("Modal frame size invalid."));
  }
}

/**
 * Process the submitted menu item edit form.
 */
function modalframe_menu_edit_menu_item_form_submit($form, & $form_state) {
  $mlid = $form_state['values']['menu']['mlid'];
  $enable = $form_state['values']['modalframe']['enable'];
  $size = $form_state['values']['modalframe']['size'];

  $options = modalframe_menu_get_options($mlid, TRUE);
  //update options
  $options['modalframe'] = $form_state['values']['modalframe'];

  //remove original classes if any
  $options['attributes']['class'] = preg_replace("/modalframe-menu mfmlid-$mlid( mfsize-\d*x\d*)*/", '', $options['attributes']['class']);
  if ($enable) {
    $classes = "modalframe-menu mfmlid-$mlid". ($size ? " mfsize-$size" : '');
    $options['attributes']['class'] = (empty ($options['attributes']['class']) ? $classes : $options['attributes']['class']." $classes");
  }
  elseif (empty ($options['attributes']['class'])) {
    unset ($options['attributes']['class']);
  }

  db_query('UPDATE {menu_links} SET options = "%s" WHERE mlid = %d', serialize($options), $form_state['values']['menu']['mlid']);
}

/**
 * Submit handler for our node edit form menu.
 *
 * This function is part of the node edit form menu.
 *
 * @see modalframe_menu_node_edit_page()
 * @see modalframe_menu_form_alter()
 */
function modalframe_menu_form_submit($form, & $form_state) {
  $mlid = $form_state['values']['mfmlid'];
  $options = modalframe_menu_get_options($mlid);
  //Close frame if the button pressed is listed in the menu item options
  foreach (explode("\n", $options['closebuttons']) as $button) {
    if (t(trim($button)) == trim($form_state['clicked_button']['#value'])) {
      modalframe_menu_close_frame($options['closemessage']);
    }
  }
}

/**
 * Close the currently open frame.
 * @param $message The message to display when the frame is closed (displayed as a JavaScript alert). Should not be translated.
 */
function modalframe_menu_close_frame($message = NULL) {
  $args = array ();
  if ($message) {
    $args['message'] = t($message);
  }
  modalframe_close_dialog($args);
}

/**
 * Retrieve the options for a menu item.
 * @param $mlid The menu link ID of the menu item.
 * @param $all Set to TRUE to retrieve all the options for a menu item (including those defined by other modules), or FALSE to retrieve only those for modalframe_menu. Default is FALSE.
 * @return An array containing the options. If $all == TRUE then the modalframe_menu options are included in array value with key 'modalframe'.
 */
function modalframe_menu_get_options($mlid, $all = FALSE) {
  $options = unserialize(db_result(db_query('SELECT options FROM {menu_links} WHERE mlid = %d', $mlid)));
  if ($all)
    return $options;
  return $options['modalframe'];
}
