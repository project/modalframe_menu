(function ($) {

/**
 * Modal Frame for Menu Items object for child windows.
 */
Drupal.modalFrameMenuChild = Drupal.modalFrameMenuChild || {
  processed: false,
  behaviors: {}
};

/**
 * Child dialog behavior.
 */
Drupal.modalFrameMenuChild.attach = function(context) {
  var self = Drupal.modalFrameMenuChild;
  // Attach child related behaviors to the current context.
  self.attachBehaviors(context);
};

/**
 * Attach child related behaviors to the iframed document.
 */
Drupal.modalFrameMenuChild.attachBehaviors = function(context) {
  $.each(this.behaviors, function() {
    this(context);
  });
};

/**
 * Add current mfmlid GET var to all links.
 */
Drupal.modalFrameMenuChild.behaviors.parseLinks = function(context) {
  var t = window.location.search;
  var mlid = window.location.search.match(/[&\?]mfmlid=(\d+)/i)[1];
  $('a:not(.modalframemenu-processed)', context).addClass('modalframemenu-processed').each(function() {
    var href = $(this).attr('href');
    
    //Do not process links with an empty href, or that only have the fragment, 
    //or that already have a menu link id defined, or external links
    if (!href || href.length <= 0 || href.charAt(0) == '#' || href.match(/\?.*mfmlid=/) || href.match(/^https?:\/\//))
      return;
    
    //add mfmlid GET var so server side knows this link came from this menu item frame
    href = href + (href.indexOf('?') == -1 ? '?' : '&') + 'mfmlid=' + mlid;
    $(this).attr('href', href);
    
    //if modalframe would modify or has modified this link
    if (!($(this).hasClass('modalframe-exclude'))) {
      //if the link has already been processed by modalframe api
      if ($(this).hasClass('modalframe-processed')) {
        //remove target = '_new' set by modalframe api
        $(this).removeAttr('target');
      }
      else {
        //otherwise prevent target being altered by modalframe api
        $(this).addClass('modalframe-exclude');
      }
    }
  });
};

/**
 * Attach our own behavior on top of the list of existing behaviors.
 *
 * We need to execute before everything else because the child frame is not
 * visible until we bind the child window to the parent, and this may cause
 * problems with other behaviors that need the document visible in order to
 * do its own job.
 */
Drupal.behaviors = $.extend({modalFrameMenuChild: Drupal.modalFrameMenuChild.attach}, Drupal.behaviors);

})(jQuery);
