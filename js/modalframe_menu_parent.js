(function($){
  Drupal.behaviors.modalFrameMenuParent = function(){
    $('.modalframe-menu:not(.modalframe-menu-processed)').addClass('modalframe-menu-processed').click(function(){
      var element = this;
      
      // This is our onSubmit callback that will be called from the child window
      // when it is requested by a call to modalframe_close_dialog() performed
      // from server-side submit handlers.
      function onSubmitCallbackMenu(args, statusMessages){
        // Display status messages generated during submit processing.
        if (statusMessages) {
          $('.modalframe-menu-messages').hide().html(statusMessages).show('slow');
        }
        
        if (args && args.message) {
          // Provide a simple feedback alert deferred a little.
          setTimeout(function(){
            alert(args.message);
          }, 500);
        }
      }
      
      // Hide the messages before opening a new dialog.
      $('.modalframe-menu-messages').hide('fast');
      
      // Build modal frame options.
      var mlid = element.className.match(/mfmlid-(\d*)/).pop(); //menu link id
      var url = $(element).attr('href');
      url = url + (url.indexOf('?') == -1 ? '?' : '&') + 'mfmlid=' + mlid;
      var modalOptions = {
        url: url,
        onSubmit: onSubmitCallbackMenu
      };
      
      // Try to obtain the dialog size from the className of the element.
      var regExp = /^.*mfsize-(\d*x\d*).*$/;
      if (typeof element.className == 'string' && regExp.test(element.className)) {
        var size = element.className.match(regExp).pop().split('x');
        modalOptions.width = parseInt(size[0]);
        modalOptions.height = parseInt(size[1]);
        modalOptions.autoFit = false;
      }
      else {
        modalOptions.autoFit = true;
      }
      
      // Open the modal frame dialog.
      Drupal.modalFrame.open(modalOptions);
      
      // Prevent default action of the link click event.
      return false;
    });
  };
})(jQuery);
