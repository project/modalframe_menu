Modal Frames from Menu Items


CONTENTS OF THIS FILE
=====================
- OVERVIEW
- REQUIREMENTS


OVERVIEW
========

This module allows loading menu item targets (the page a menu item links to) 
in a modal frame. 

Extra configuration items are added to menu item editing forms:

  Open in a modal frame
    Check this to enable opening the menu item in a modal frame.
    
  Buttons to close on
    If the linked page contains a form, a list of buttons which when clicked should close the frame (the frame only closes on successful form validation).
    
  Pages to close on
    A list of page addresses which if loaded in the frame will cause the frame to close.
  
  Close message
    A message to display when the frame is closed.
    
  Buttons to remove
    If the linked page contains a form, a list of buttons to remove from the form.
    
  Frame size
    Allows setting the frame size in pixels, leave blank to allow the frame to fit to the content.


REQUIREMENTS
============

Modal Frame API is used to generate and work with the modal frames (http://drupal.org/project/modalframe).
